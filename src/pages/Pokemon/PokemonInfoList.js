import { Tree } from 'antd';
import { connect } from 'react-redux';
import getParsedInfo from './getParsedInfo';

const PokemonInfoList = ({pokemon}) => {
  const treeData = getParsedInfo(pokemon)

  return (
    <Tree
      defaultExpandedKeys={['0-0-0', '0-0-1']}
      defaultSelectedKeys={['0-0-0', '0-0-1']}
      defaultCheckedKeys={['0-0-0', '0-0-1']}
      treeData={treeData}
      style={{marginTop: '20px'}}
    />
  );
};
const mapStateToProps = ({pokemon}) => ({pokemon});
export default connect(mapStateToProps)(PokemonInfoList)