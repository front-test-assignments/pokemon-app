const getParsedInfo = (test, path = '0', level = 0) => {
    const list = [];
    for (let prop in test) {
      if (typeof(test[prop]) === 'string' || typeof(test[prop]) === 'number') {
        const key = `${path}-${level}`;
        const treeNode = {
          title: firstSymbolToUpperCase(`${prop}: ${test[prop]}`),
          key,
        };
        list.push(treeNode);
      }
      else {
        if (typeof(test[prop][0]) === 'string') {
          const key = `${path}-${level}`;
          const treeNode = {
            title: firstSymbolToUpperCase(prop),
            key,
          };
          treeNode.children = test[prop].map((element, i) => {
            return {
              title: firstSymbolToUpperCase(element),
              key: `${key}-${i}`
            }
          })
          list.push(treeNode);
        }
        else {
          const key = `${path}-${level}`;
          const treeNode = {
            title: firstSymbolToUpperCase(prop),
            key,
          };
          treeNode.children = getParsedInfo(test[prop], key);
          list.push(treeNode)
        }
      }
      level +=1
    }
    return list;
  }
  const firstSymbolToUpperCase = (string) => {
      const str = string.slice(1)
      return string[0].toUpperCase().concat(str)
  }
export default getParsedInfo