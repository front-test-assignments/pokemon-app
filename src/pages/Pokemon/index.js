import { useEffect } from "react"
import { fetchPokemonById } from "../../store/thunks"
import { connect } from 'react-redux'
import { useParams } from "react-router"
import { Button } from 'antd'
import { Link } from 'react-router-dom';
import LoadingScreen from "../../components/LoadingScreen"
import ApiErrorMessage from "../../components/ApiErrorMessage"
import PokemonInfoList from "./PokemonInfoList"


const Pokemon = ({ dispatch, pokemon, isLoading, isError }) => {
  const { id } = useParams()

  const reloadClickHandle = () => {
    window.location.reload()
  }

  useEffect(() => {
    dispatch(fetchPokemonById(id))
  }, [])
  
  const renderPokemonPage = () => {
    if (!pokemon) {
      return <LoadingScreen/>
    }
    
    return (
      <div style={{
        display: 'flex', 
        flexDirection:'column', 
        alignContent: 'center'
      }}>
        <div style={{
          width: '100%', 
          height:'50px', 
          backgroundColor:'#4dcde6', 
          alignContent:'flex-end'
        }}>
          <Button size={'small'} style={{marginTop:'12px', marginLeft:'12px'}}>
            <Link to={`/`}>Home</Link>
          </Button>
          <Button size={'small'} style={{marginTop:'12px', marginLeft:'12px'}} onClick={reloadClickHandle}>
            Update
          </Button>
        </div>
        <div style={{justifyContent: 'center', display: 'flex', marginTop:'20px'}}>
          <div style={{display:'flex', flexDirection:'row'}}>
            <img 
              src={pokemon.imageUrlHiRes} 
              alt={pokemon.id} 
              style={{width:'350px', height:'500px'}}
            >
            </img>
            <PokemonInfoList/>
          </div>
        </div>
      </div>
    )
  }

  if (isError) {
    return (<ApiErrorMessage/>)
  }

  if (isLoading) {
    return (
      <LoadingScreen/>
    )
  }

  return renderPokemonPage()
}

const mapStateToProps = ({pokemon, isLoading, isError}) => ({pokemon, isLoading, isError});
export default connect(mapStateToProps)(Pokemon);
