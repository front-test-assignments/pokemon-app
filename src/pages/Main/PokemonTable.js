import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux'

const PokemonTable = ({ pokemonsList }) => {
  return (
    <>
      {pokemonsList.map((pokemon) => {
        return (
          <div style={{
            display:'flex', 
            flexDirection:'row', 
            marginLeft:'20px', 
            marginTop:'20px'
          }}>
            <div style={{width:'370px', height: 'auro'}}>
              <img 
                src={pokemon.imageUrl} 
                alt={pokemon.id} 
                style={{width:'350px'}}/>
            </div>
            <div style={{display:'flex', flexDirection:'column', marginTop:'20px'}}>
              <h3><Link to={`/details/${pokemon.id}`}>{pokemon.name}</Link></h3>
              <p>Supertype:   {pokemon.supertype}</p>
              <p>Subtype:     {pokemon.subtype}</p>
              <p>EvolvesFrom: {pokemon.evolvesFrom}</p>
              <p>Hp:          {pokemon.hp}</p>
              <p>Number:      {pokemon.number}</p>
              <p>Artist:      {pokemon.artist}</p>
              <p>Rarity:      {pokemon.rarity}</p>
              <p>Series:      {pokemon.series}</p>
            </div>
          </div>
        )
      })}
    </>
  )
  
}
const mapStateToProps = ({pokemonsList}) => ({ pokemonsList })
export default connect(mapStateToProps)(PokemonTable)
 