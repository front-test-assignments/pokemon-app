import { useEffect } from 'react';
import { connect } from 'react-redux';
import { Button } from 'antd'
import ApiErrorMessage from '../../components/ApiErrorMessage';
import LoadingScreen from '../../components/LoadingScreen';
import { fetchPokemonsList } from '../../store/thunks';
import PokemonTable from './PokemonTable';

const Main = ({ dispatch, isLoading, isError }) => {
  useEffect(() => dispatch(fetchPokemonsList()), [])
  
  if (isLoading) {
    return (<LoadingScreen/>)
  }
  if (isError) {
    return (<ApiErrorMessage/>)
  }

  const reloadClickHandle = () => {
    window.location.reload()
  }

  return (
    <>
      <div style={{
        width: '100%', 
        height:'50px', 
        backgroundColor:'#4dcde6', 
        alignContent:'flex-end'
      }}>
        <Button 
          size={'small'} 
          style={{marginTop:'12px', marginLeft:'12px'}} 
          onClick={reloadClickHandle}
        >
          Update
        </Button>
      </div>
      <PokemonTable/>
    </>
  )
}

const mapStateToProps = ({isLoading, isError}) => ({isLoading, isError})
export default connect(mapStateToProps)(Main)