import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import { Provider } from 'react-redux';
import './App.css';
import Main from './pages/Main';
import Pokemon from './pages/Pokemon'
import store from './store/store'

function App() {
  return (
    <Provider store={store}>
      <Router>
        <Switch>
          <Route exact path="/"><Main /></Route>
          <Route exact path="/details/:id"><Pokemon /></Route>
        </Switch>
      </Router>
    </Provider>
  );
}

export default App;