export const SET_POKEMONS_LIST = 'SET_POKEMONS_LIST';
export const setPokemonsList = (pokemonsList) => ({ type: SET_POKEMONS_LIST, pokemonsList});
export const SET_POKEMON = 'SET_POKEMON';
export const setPokemon = (payload) => ({ type: SET_POKEMON, payload});
export const SET_IS_LOADING = 'SET_IS_LOADING';
export const setIsLoading = (isLoading) => ({ type: SET_IS_LOADING, isLoading});
export const SET_IS_ERROR = 'SET_IS_ERROR';
export const setIsError = (isError) => ({ type: SET_IS_ERROR, isError});
