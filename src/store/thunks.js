import axios from 'axios';
import { setPokemon, setPokemonsList, setIsLoading, setIsError } from './actions'

export const fetchPokemonById = (pokemonID) => {
  return (dispatch, _getState) => {
    dispatch(setIsLoading(true))
    const requestStr = `https://api.pokemontcg.io/v1/cards/${pokemonID}`;
    axios.get(requestStr).then(({data}) => {
        dispatch(setPokemon({pokemon: data.card}));
        dispatch(setIsLoading(false))
    }).catch(() => {
      dispatch(setIsLoading(false))
      dispatch(setIsError(true))
    })
  }
}

export const fetchPokemonsList = () => {
  return (dispatch) => {
    dispatch(setIsLoading(true))
    const requestStr = `https://api.pokemontcg.io/v1/cards`;
    axios.get(requestStr).then((response) => {
        dispatch(setPokemonsList(response.data.cards))
        dispatch(setIsLoading(false))
    }).catch(() => {
        dispatch(setIsLoading(false))
        dispatch(setIsError(true))
    })
  }
}