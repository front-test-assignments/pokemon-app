import { SET_IS_ERROR, SET_IS_LOADING, SET_POKEMON, SET_POKEMONS_LIST } from "./actions";

const initState = {
    isError: false,
    isLoading: false,
    pokemon: {}, 
    pokemonsList: []
}

export const rootReducer = (state = initState, action) => {
    switch(action.type) {
        case SET_POKEMON: {
            const { payload: { pokemon } } = action
            return {
                ...state,
                pokemon, 
            }
        }
        case SET_POKEMONS_LIST: {
            const pokemonsList  = action.pokemonsList
            return {
                ...state,
                pokemonsList,
            }
        }
        case SET_IS_LOADING: {
            const { isLoading } = action
            return {
                ...state,
                isLoading
            } 
        }
        case SET_IS_ERROR: {
            const { isError } = action
            return {
                ...state,
                isError
            } 
        }
        default:
            return state
    }
}
