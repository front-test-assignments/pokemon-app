const ApiErrorMessage = () => {
  const reloadClickHandle = () => {
    window.location.reload()
  }
  return (
    <div>
      <h3>Произошла ошибка при обращения к API или превышен лимит обращений</h3>
      <button onClick={reloadClickHandle}>Попробовать снова</button> 
    </div>
  )
}

export default ApiErrorMessage